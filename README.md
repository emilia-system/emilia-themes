# Emilia Themes

[![pipeline status](https://gitlab.com/emilia-system/emilia-themes/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-themes/commits/master)
[![License](https://img.shields.io/badge/license-GPL2-yellow)](https://choosealicense.com/licenses/gpl-2.0/)

This repository has the code for the Daisy (Emilia's main theme), and the extra themes, with new flowers to choose.

They are combinations of 
[Adapta GTK](https://github.com/adapta-project/adapta-gtk-theme) and 
[Adapta QT (KDE)](https://github.com/PapirusDevelopmentTeam/adapta-kde).

## Images

Check both Adapta GTK and KDE for appearance. The only changes Emilia Team did was to change the colors. The diferential of this theme is also to combine both the themes.

For a simple example, the Dolphin file manager(Qt/KDE) and Nemo file manager(GTK) running side by side.
If curious, Emilia uses and recomends the Papirus Icon theme.

![Dolphin and Nemo file manager, looking like belongs in the same system](readme-images/nemo-and-dolphin.png)

And now, in dark mode.

![Dolphin and Nemo file manager, looking like belongs in the same system, but in dark mode](readme-images/nemo-and-dolphin-dark.png)

## Color table

| Theme                        | Primary (Selection) | Secondary (Accent) | Terciary (Suggestion) | Quaternary (Destruction) |
| :--------------------------- | :------------------ | :----------------- | :-------------------- | :----------------------- |
| Adapta (Base)    | `#00BCD4` | `#4DB6AC` | `#009688` | `#FF5252`   |
| Daisy (Main)     | `#FF9800` | `#FF5252` | `#FF8A80` | Not Changed |
| Lavander         | `#BA68C8` | `#CE93D8` | `#EA80FC` | Not Changed |
| Lilypad          | `#8BC34A` | `#64DD17` | `#76FF03` | Not Changed |
| Gentilian        | `#42A5F5` | `#64B5F6` | `#82B1FF` | Not Changed |
| Rose             | `#FF5252` | `#EF9A9A` | `#E57373` | Not Changed |
| Sunflower        | `#F9A825` | `#8D6E63` | `#795548` | Not Changed |
| Sakura           | `#FF4081` | `#F48FB1` | `#F8BBD0` | Not Changed |
| QueenOfTheNight  | `#9E9E9E` | `#BDBDBD` | `#FAFAFA` | Not Changed |

## Installation

In the package Emilia Tools, lies the code that unite both Adaptas, changing the colors to the desired ones, so install that before this one. 
Unfortunately, the compilation of this theme is the same as the Adapta GTK. Therefore, the compiling time is similar.

Use make daisy or make extra to make the individual packages, or use only make, for everyone.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[GPL2](https://choosealicense.com/licenses/gpl-2.0/)


# Emilia Themes - pt_BR

Este repositório contém o código para Daisy (Tema principal da Emilia), e para os temas extras, com novas flores para escolher.

São combinações do 
[Adapta GTK](https://github.com/adapta-project/adapta-gtk-theme) e do 
[Adapta QT (KDE)](https://github.com/PapirusDevelopmentTeam/adapta-kde).

## Imagens

Cheque ambos Adapta GTK e KDE pela aparência. As únicas mudanças que o Time Emilia fez foi a troca de cores. O diferencial deste tema é também unir ambos.

Para um exemplo simples, o gerenciador de arquivos Dolphin e o Nemo rodando um do lado do outro. Caso esteja curioso, Emilia usa e recomenda o tema de icones Papirus.

![Dolphin e nemo, parecendo como se pertencessem ao mesmo sistema.](readme-images/nemo-and-dolphin.png)

E agora, no tema escuro.

![Dolphin e nemo, parecendo como se pertencessem ao mesmo sistema, mas no tema escuro](readme-images/nemo-and-dolphin-dark.png)

## Instalação

No pacote Emilia Tools, há o código que pega ambos Adaptas e os unem, mudando as cores para aquelas desejadas, então instale ele antes. 
Infelizmente, a compilação deste tema é a mesma do Adapta GTK. Portanto, o tempo de compilação é similar.

Use make daisy ou make extra para criar os pacotes individuais, ou use apenas make, para todos.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL2](https://choosealicense.com/licenses/gpl-2.0/)
